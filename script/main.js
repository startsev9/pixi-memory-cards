let Application			= PIXI.Application,
	Container			= PIXI.Container,
	loader				= PIXI.loader,
	resources			= PIXI.loader.resources,
    TextureCache		= PIXI.utils.TextureCache,
	Graphics			= PIXI.Graphics,
	Sprite				= PIXI.Sprite,
	Text				= PIXI.Text,
	TextStyle			= PIXI.TextStyle,
	Texture 			= PIXI.Texture;


const app = new Application({
    width: 800, height: 600, backgroundColor: 0x1a1a1f, resolution: window.devicePixelRatio || 1,
});
document.body.appendChild(app.view);

var id, counter = 0, bestResult = 99999999;
var images = [ "ak1.png", "ak2.png", "awp1.png", "awp2.png", "m4-1.png", "m4-2.png", "deagle1.png", "deagle2.png", "ak1.png", "ak2.png", "awp1.png", "awp2.png", "m4-1.png", "m4-2.png", "deagle1.png", "deagle2.png"];
var card, cards = [], pairCount = 0;
var time = 0, startTimer = 500, timeText;
var firstCard, secondCard, timeText = undefined;
var hasFlippedCard = false;
var lockBoard = false;
var restartFlag = false;
var restartButton;
var seconds = startTimer / 100;
const pairConst = 8, cardsAmount = 16, rows = columns = 4;

loader
	.add("../images/atlas.json")
	.load(setup);

function setup() {
	id = resources["../images/atlas.json"].textures;

	shuffle(images);

	const timeBack = new Sprite.from(id["actimeBack.png"]);
	const cardBack = new Sprite.from(id["case.png"]);
	const restartButtonSprite = new Sprite.from(id["restartButton.png"]);
	const bestResultBack = new Sprite.from(id["bestResultBack.png"]);
	restartButton = restartButtonSprite;

	restartButton.anchor.set(0.5);
	restartButton.interactive = true;
	restartButton.buttonMode = true;
	restartButton
		.on('pointerdown', onButtonDown)
		.on('pointerup', onRestartButtonUp)
		.on('pointerupoutside', onRestartButtonUp)
		.on('pointerover', onButtonOver)
		.on('pointerout', onButtonOut);

	restartButton.position.set(720, 500);
	app.stage.addChild(restartButton);



	let bestResultContainer = new PIXI.Container();
	bestResultContainer.addChild(bestResultBack);

	let textStyle = new PIXI.TextStyle({
		fontFamily: "Arial",
		fontSize: 20,
		fill: "white",
	});

	bestResultText = new PIXI.Text('Best result: -', textStyle);
	bestResultText.anchor.set(0.5);
	bestResultText.position.set(bestResultBack.width / 2, bestResultBack.height / 2);
	bestResultContainer.addChild(bestResultText);
	bestResultContainer.position.set(523.8, 0);
	
	app.stage.addChild(bestResultContainer);


	for(let i = 0; i < columns; i++){	
		for (let j = 0; j < rows; j++) {
			name = images[counter];
			const cardFace = new Sprite.from(id[name]);

			card = cardFace;
			card.name = name;
			card.anchor.set(0.5);
			card.interactive = false;
			card.buttonMode = false;
			card.x = 200 + j * 128;
			card.y = 110 + i * 128;

			card
		        .on('pointerdown', onButtonDown)
		        .on('pointerup', onButtonUp)
		        .on('pointerupoutside', onButtonUp)
		        .on('pointerover', onButtonOver)
		        .on('pointerout', onButtonOut);
		    cards[counter] = card;

			app.stage.addChild(cards[counter]);			
			counter++;
		}
	}
	let timeContainer = new PIXI.Container();
	timeContainer.addChild(timeBack);

	timeText = new PIXI.Text(startTimer/100, textStyle);
	timeText.anchor.set(0.5);
	timeText.position.set(timeBack.width / 2, timeBack.height / 2);
	timeContainer.addChild(timeText);
	
	app.stage.addChild(timeContainer);

	timer(startTimer);
	start();
}

//// Game functions ////



async function start() {

	await sleep(5000);

	restartButton.interactive = true;
	restartButton.buttonMode = true;

	for(var i = 0; i < cardsAmount; i++) {
		cards[i].interactive = true;
		cards[i].buttonMode = true;
		cards[i].texture = id["case.png"];
	}
	pairCount = 0;
}

async function restart() {
	restartFlag = true;

	shuffle(images);
	for(let i = 0; i < cardsAmount; i++) {
		name = images[i];
		
		cards[i].interactive = false;
		cards[i].buttonMode = false;
		cards[i].texture = id[name];
		cards[i].name = name;
	}

	start();
}

function shuffle(images) {
	for (let i = images.length - 1; i > 0; i--) {
	    let j = Math.floor(Math.random() * (i + 1));
	    [images[i], images[j]] = [images[j], images[i]];
	}
	return images;
}

//// Button functions ////

function onButtonDown() {
    this.scale.set(0.95);
}

function onButtonUp() {
    this.scale.set(1);
    flipCard(this);
}

function onButtonOver() {
    this.scale.set(1.03);
}

function onButtonOut() {
    this.scale.set(1);
}

function onRestartButtonUp() {
	this.scale.set(1);
	restartButton.interactive = false;
	restartButton.buttonMode = false;
	restart();
}

//// Card flip functions ////

function flipCard(card) {
	if(lockBoard) return;
	if (card === firstCard) return;

	card.texture = id[card.name];

	if(!hasFlippedCard) {
		hasFlippedCard = true;
		firstCard = card;
		firstCard.name = card.name;
		firstCard.interactive = false;
		firstCard.buttonMode = false;
		return;
	}
	
	secondCard = card;
	secondCard.name = card.name;

	checkForMatch();
}

function checkForMatch() {
	let isMatch = firstCard.name === secondCard.name;

	isMatch ? disableCards() : unflipCards();
}

function disableCards() {
	firstCard.interactive = false;
	secondCard.interactive = false;
	firstCard.buttonMode = false;
	secondCard.buttonMode = false;

	pairCount++;
	if(pairCount === pairConst){
		Math.abs(seconds) < bestResult ? bestResult = Math.abs(seconds) : bestResult;
		bestResultText.text = bestResult < 10 ? `Best result: ${bestResult} second` : `Best result: ${bestResult} seconds`;
		restart();
	}
	resetBoard();
}

async function unflipCards() {
	lockBoard = true;

	await sleep(500)

	firstCard.interactive = true;
	firstCard.buttonMode = true;
	secondCard.interactive = true;
	secondCard.buttonMode = true;
	firstCard.texture = id["case.png"];
	secondCard.texture = id["case.png"];

	resetBoard();
}

function resetBoard() {
	[hasFlippedCard, lockBoard] = [false, false];
	[firstCard, secondCard] = [null, null];
}

//// Help functions ////

function timer() {
    setInterval(function() {
        if (restartFlag === false) {
            seconds --;
            timeText.text = String(Math.abs(seconds));
        }
        else {
        	restartFlag = false;
        	seconds = startTimer / 100;
        	timeText.text = String(Math.abs(seconds));
        }
    }, 1000);
}

function sleep(ms) {
 	return new Promise(resolve => setTimeout(resolve, ms));
}